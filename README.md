# Sample Project for JB Interview

## Developer Setup

Follow the steps below to run the server in your local machine.

1. Install Node.js & Postgres
2. Install NPM
3. checkout code from `git@bitbucket.org:ningsuhen/jba.git`
4. Run `npm install` to install package dependencies.
5. Create Postgres table and import tables.sql & data.sql. 
```
createdb jba-dev
psql jba-dev
\i [path to]/tables.sql
\i [path to]/data.sql
```
6. Run `npm start` or `npm run start_dev` from project root
7. Open http://localhost:3000

## Running Tests.

1. Run `npm test` from project root
